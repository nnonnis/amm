<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of data_manager
 *
 * @author Nicola
 */
class Data_manager {

    private $idLibro;
    private $idUtente;
    private $idVenditore;
    private $idCliente;
    private $idDeposito;
    private $isbn;
    private $quantita;
    private $prezzo;
    private $costo;
    private $titolo;
    private $idArray;

    public function __construct() {
        ;
    }

    public function setArray($idArray) {
        $this->idArray = $idArray;
    }

    public function getArray() {
        return $this->idArray;
    }

    public function setIdUtente($idUtente) {
        $this->idUtente = $idUtente;
    }

    public function getTitolo() {
        return $this->titolo;
    }

    public function setTitolo($titolo) {
        $this->titolo = titolo;
    }

    public function getIdUtente() {
        return $this->idUtente;
    }

    public function getIdLibro() {
        return $this->idLibro;
    }

    public function setIdLibro($idLibro) {
        $this->idLibro = $idLibro;
    }

    public function setIdVenditore($idVenditore) {
        $this->idVenditore = $idVenditore;
    }

    public function getIdVenditore() {
        return $this->idVenditore;
    }

    public function setIdCliente($idCliente) {
        $this->idCliente = $idCliente;
    }

    public function getIdCliente() {
        return $this->idCliente;
    }

    public function setIdDeposito($idDeposito) {
        $this->idDeposito = $idDeposito;
    }

    public function getIdDeposito() {
        return $this->idDeposito;
    }

    public function setIsbn($isbn) {
        $this->isbn = $isbn;
    }

    public function getIsbn() {
        return $this->isbn;
    }

    public function setQuantita($quantita) {
        $this->quantita = $quantita;
    }

    public function getQuantita() {
        return $this->quantita;
    }

    public function setPrezzo($prezzo) {
        $this->prezzo = $prezzo;
    }

    public function getPrezzo() {
        return $this->prezzo;
    }

    public function setCosto($costo) {
        $this->costo = $costo;
    }

    public function getCosto() {
        return $this->costo;
    }

}
