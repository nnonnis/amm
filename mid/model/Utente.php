<?php

class Utente {

    private $username;
    private $password;
    private $nome;
    private $cognome;
    private $citta;
    private $provincia;
    private $via;
    private $cap;
    private $civico;
    private $email;
    private $telefono;
    private $utente;
    private $id;
    
    const Amministratore = 1;
    const Commerciante = 2;
    const Cliente = 3;
    
    public function __construct() {
        ;
    }

        public function esiste() {
        return isset($this->utente);
    }

        public function uguale(User $user) {

        return  $this->id == $user->id &&
                $this->nome == $user->nome &&
                $this->cognome == $user->cognome &&
                $this->ruolo == $user->ruolo;
    }
    /*NOME FUNZIONI SOPRASTANTI DA MOFICARE*/
    
    
    //restituisce la password
    public function getPassword() {
        return $this->password;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getCognome() {
        return $this->cognome;
    }

    public function getCitta() {
        return $this->citta;
    }

    public function getVia() {
        return $this->via;
    }

    public function getCivico() {
        return $this->civico;
    }

    public function getCap() {
        return $this->cap;
    }

    public function getProvincia() {
        return $this->provincia;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function getTipo_ut() {
        return $this->utente;
    }

    public function getId() {
        return $this->id;
    }

    public function setUsername($username) {
        if (!filter_var($username, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/[a-zA-Z0-9]{5,}/')))) {
            return false;
        }
        $this->username = $username;
        return true;
    }


    public function setPassword($password) {
        $this->password = $password;
        return true;
    }
    
    public function setNome($nome){
        if(!filter_val($nome,FILTER_VALIDATE_REGEXP, array('options' => array('regexp' =>'/[a-zA-Z]{3,}/')))) {
            return false;
        }
        $this->nome =$nome;
        return true;
    }

        public function setCognome($cognome){
        if(!filter_val($cognome,FILTER_VALIDATE_REGEXP, array('options' => array('regexp' =>'/[a-zA-Z]{3,}/')))) {
            return false;
        }
        $this->cognome =$cognome;
        return true;
    }
    
            public function setCitta($citta){
        if(!filter_val($citta,FILTER_VALIDATE_REGEXP, array('options' => array('regexp' =>'/[a-zA-Z]/')))) {
            return false;
        }
        $this->citta =$citta;
        return true;
    }
    
        public function setVia($via){
        if(!filter_val($via,FILTER_VALIDATE_REGEXP, array('options' => array('regexp' =>'/[a-zA-Z]/')))) {
            return false;
        }
        $this->via =$via;
        return true;
    }
    
        public function setCivico($civico){
        if(filter_val($civico,FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE)) {
            $this->civico=$civico;
            return true;
        }
        return false;
    }
    
        public function setCap($cap){
        if(!filter_val($cap,FILTER_VALIDATE_REGEXP, array('options' => array('regexp' =>'/[0-9]{5}/')))) {
            return false;
        }
        $this->cap =$cap;
        return true;
    }
        public function setProvincia($provincia){
        if(!filter_val($provincia,FILTER_VALIDATE_REGEXP, array('options' => array('regexp' =>'/[a-zA-Z]{2}/')))) {
            return false;
        }
        $this->provincia =$provincia;
        return true;
    }
        public function setEmail($email){
        if(!filter_val($email,FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        $this->email =$email;
        return true;
    }
        public function setId($id){
        if(!filter_val($id,FILTER_VALIDATE_INT)) {
            return false;
        }
        $this->id =$id;
        return true;
    }
    
        public function setTelefono($telefono){
        if(!filter_val($telefono,FILTER_VALIDATE_REGEXP, array('options' => array('regexp' =>'/[0-9]{7,}/')))) {
            return false;
        }
        $this->telefono =$telefono;
        return true;
    }
    
        public function setTipo_ut($utente) {
        switch ($utente) {
            case self::Amministratore:
            case self::Commerciante:
            case self::Cliente:
                $this->utente = $utente;
                return true;
            default:
                return false;
        }
    }
}

?>
