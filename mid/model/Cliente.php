<?php
include_once 'Utente.php';


class Cliente extends Utente{

    private $deposito;
    
        public function __construct() {

        parent::__construct();
        $this->setUtente(Utente::Cliente);
        
    }
    
    public function setDeposito($deposito){
        if(!filter_var($deposito,FILTER_VALIDATE_INT,FILTER_NULL_ON_FAILURE)){
            return false;
        }
        $this->deposito = $deposito;
        return true;
    }
    
    public function getDeposito(){
            return $this->deposito;
    }
}
?>
