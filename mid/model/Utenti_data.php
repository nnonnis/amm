<?php

include_once 'Utente.php';
include_once 'Cliente.php';
include_once 'Commerciante.php';
//include_once 'Archivio_acq.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utenti_data
 *
 * @author amm
 */
class Utenti_data {

    private function __construct() {
        ;
    }

    public static function caricaUtente($username, $password) {
        if ($username == 'admin' && $password = 'admin') {
            $us = new Utente();
            $us->setUsername($username);
            $us->setPassword($password);
            $us->setTipologia_utente(Utente::Amministratore);
            $us->setNome('Nicola');
            $us->setCognome('Nonnis');
            $us->setId(0);
            $us->setEmail('nonnisnicola@gmail.com');
            $us->setCap(09040);
            $us->setCitta('SIURGUS DONIGALA');
            $us->setVia('via curatoria');
            $us->setCivico(21);
            $us->setProvincia('CA');
            $us->setTelefono('3408665974');

            return $us;
        } else if ($username == 'cliente' && $password == 'cli') {
            $us = new Cliente();
            $us->setUsername($username);
            $us->setPassword($password);
            $us->setTipologia_utente(Utente::Cliente);
            $us->setNome('Mario');
            $us->setCognome('Monti');
            $us->setId(1);
            $us->setEmail('mariomonti@gov.it');
            $us->setCap(09100);
            $us->setCitta('CAGLIARI');
            $us->setVia('via dante');
            $us->setCivico(218);
            $us->setProvincia('CA');
            $us->setTelefono('3426998955');
            return $us;
        } else if ($username == 'commerciante' && $password == 'com') {

            $us = new Commerciante();
            $us->setUsername($username);
            $us->setPassword($password);
            $us->setRuolo(Utente::Commerciante);
            $us->setId(2);
            $us->setNome('Gianni');
            $us->setCognome('Sanna');
            $us->setEmail('g.sanna@librerie.com');
            $us->setCap(08601);
            $us->setCitta('GARBAGNATE MILANESE');
            $us->setVia('via europa');
            $us->setCivico(114);
            $us->setProvincia('MI');
            $us->setTelefono('0225165997');
            return $us;
        }
    }

    public static function &getListaCommercianti() {
        $comm1 = new Commerciante();
        $comm1->setNome('Mario');
        $comm1->setCognome('Rossi');
        $comm1->setAzienda('HobbyesBooks');
        $comm1->setDescrizioneazienda('Vendita di libri cartacei destinati all hobbistica');
        $comm1->setFondo_azienda(0);
        $comm1->setId(3);

        $comm2 = new Commerciante();
        $comm2->setNome('Maria');
        $comm2->setCognome('Bianca');
        $comm2->setAzienda('onlinebooks');
        $comm2->setDescrizioneazienda('Vendita eBooks e spartiti musicali');
        $comm2->setFondo_azienda(0);
        $comm2->setId(4);

        $comm3 = new Commerciante();
        $comm3->setNome('Luca');
        $comm3->setCognome('Marrotto');
        $comm3->setAzienda('PassioneLettura');
        $comm3->setDescrizioneazienda('Vendita di romanzi, manuali ed enciclopedie');
        $comm3->setFondo_azienda(0);
        $comm3->setId(5);

        $comm4 = new Commerciante();
        $comm4->setNome('Gianni');
        $comm4->setCognome('Sanna');
        $comm4->setAzienda('Libroteka');
        $comm4->setDescrizioneazienda('Copisteria, tipografica, vendita libri al dettaglio');
        $comm4->setFondo_azienda(0);
        $comm4->setId(6);

        $commercianti = array();
        $commercianti[] = $comm1;
        $commercianti[] = $comm2;
        $commercianti[] = $comm3;
        $commercianti[] = $comm4;

        return $commercianti;
    }

    public function &getListaClienti() {
        $cli1 = new Cliente();
        $cli1->setNome('Mario');
        $cli1->setCognome('Monti');
        $cli1->setFondoCliente(100);
        $cli1->setId(1);

        $cli2 = new Cliente();
        $cli2->setNome('Marino');
        $cli2->setCognome('Manca');
        $cli2->setFondoCliente(130);
        $cli2->setId(7);

        $cli3 = new Cliente();
        $cli3->setNome('Sara');
        $cli3->setCognome('Rossi');
        $cli3->setFondoCliente(560);
        $cli3->setId(8);

        $cli4 = new Cliente();
        $cli4->setNome('Pierluigi');
        $cli4->setCognome('Greggio');
        $cli4->setFondoCliente(35);
        $cli4->setId(9);

        $cli5 = new Cliente();
        $cli5->setNome('Gina');
        $cli5->setCognome('Marcialis');
        $cli5->setFondoCliente(150);
        $cli5->setId(10);
        
        $clienti = array();
        $clienti[]=$cli1;
        $clienti[]=$cli2;
        $clienti[]=$cli3;
        $clienti[]=$cli4;
        $clienti[]=$cli5;
        return $clienti;
    }

}

?>
