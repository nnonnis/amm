<?php

include_once 'Utente.php';

class Commerciante extends Utente {

    private $azienda;
    private $descrizione;
    private $deposito;

    public function __construct() {
        parent::__construct();
        $this->setUtente(Utente::Commerciante);
    }

    public function getAzienda() {
        return $this->azienda;
    }

    public function getDescrizione() {
        return $this->descrizione;
    }

    public function getDeposito() {
        return $this->deposito;
    }

    public function setAzienda($azienda) {
        $this->azienda = $azienda;
        return true;
    }

    public function setDeposito($deposito) {
        if (!filter_val($deposito, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE)) {
            return false;
        }
        $this->deposito = $deposito;
        return true;
    }
    
    public function setDescrizione($descrizione){
        if(!filter_val($descrizione,FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/[a-zA-Z0-9]{10,}/')))){
            return false;
        }
        $this->descrizione=$descrizione;
    }
}

?>
