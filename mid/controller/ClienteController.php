<?php

include_once basename(__DIR__) . '/../view/PageStruct.php';
include_once basename(__DIR__) . '/../model/Data_manager.php';
include_once basename(__DIR__) . '/../view/connessione.php';
include_once basename(__DIR__) . '/../controller/BaseController.php';
include_once basename(__DIR__) . '/../model/Settings.php';

class ClienteController extends BaseController {

    public function __construct() {
        
    }

    public function handleInput(&$request, &$session) {

        $browser = new PageStruct(); //imposto struttura pagina

        $browser->setPagina($request['page']);

        if (!$this->loggedIn()) { //se non loggato
            $this->showLoginPage($browser);
        } else { 
            $user = $_SESSION['user'];

            if (isset($request['subpage'])) {  //se è stata richiesta una pagina
                switch ($request['subpage']) {

                    case 'compra':
                        $browser->setSottoPagina('compra');
                        break;

                    case 'anagrafica':
                        $browser->setSottoPagina('anagrafica');
                        break;


                    case 'cronologia':
                        $browser->setSottoPagina('cronologia');
                        break;

                    case 'ricarica': //pagina di ricarica del credito
                        $browser->setSottoPagina('ricarica');
                        break;

                    case 'ricarica1':
                        $browser->setSottoPagina('ricarica1');
                        break;

                    case 'ricerca':
                        $browser->setSottoPagina('ricerca');
                        break;


                    default :
                        $browser->setSottoPagina('not-found-content');
                        break;
                }
            }


            $query_idCli = mysql_query("select idCliente from clienti where utenti_id = '{$_SESSION['id']}'"); //ottengo id cliente
            if (!$query_idCli) {
                echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
                exit;
            }
            $idCliente = mysql_result($query_idCli, 0);

            Data_manager::setIdCliente($idCliente);  //imposto id cliente nel model
            Data_manager::setIdUtente($_SESSION['id']); //imposto id utente nel model

            if (isset($request['cmd'])) {   //se è stato richiamato un comando
                switch ($request['cmd']) {


                    case 'mod_pass': //modifica password
                        $new_password = $request['password'];
                        BaseController::modificaPassword($_SESSION['id'], $new_password); //richiamo la funzione che modifica la password
                        break;

                    case 'mod_domicilio': //modifica domicilio
                        $new_citta = $request['citta'];
                        $new_via = $request['via'];
                        $new_cap = $request['cap'];
                        $new_numero = $request['numero'];
                        $new_telefono = $request['telefono'];
                        $new_email = $request['email'];
                        //richiamo la funzione che modifica il domicilio
                        BaseController::modificaDomicilio($_SESSION['id'], $new_citta, $new_via, $new_cap, $new_numero, $new_telefono, $new_email);
                        break;
                }
            }
            $this->showHomeUtente($browser);
            require basename(__DIR__) . '/../view/master.php';
        }
    }

///////FUNZIONI ///////


    private function ricercaIdLibroDaTitolo($titolo) { //restituisce la variabile con la query dell'id del libro dato il titolo
        $query_search = mysql_query("select idLibro from libro where titolo = '{$titolo}'");
        if (!$query_search) {
            echo 'Impossibile eseguire la ricerca del libro nel db: ' . mysql_error();
            exit;
        }
        if (mysql_num_rows($query_search) > 0) {
            return mysql_result($query_search, 0, "idLibro");
        } else
            return 0;
    }


    private function getIdFromIsbn($isbn) { //restituisce id del libro dato l'isbn

        $query_idLibro = mysql_query("select idLibro from libro where isbn = '{$isbn}' ");
        if (!$query_idLibro) {
            echo 'Impossibile eseguire la ricerca di idLibro da isbn: ' . mysql_error();
            exit;
        }
        if (mysql_num_rows($query_idLibro) > 0) {
            $record = mysql_result($query_idLibro, 0, "idLibro");
            return $record;
        } else
            return 0;
    }

    private function getIsbnFromId($id) { //restituisce isbn dato l'id
        $query_isbnLibro = mysql_query("select isbn from libro where idLibro = '{$id}' ");
        if (!$query_isbnLibro) {
            echo 'Impossibile eseguire la ricerca di idLibro da isbn: ' . mysql_error();
            exit;
        }
        if (mysql_num_rows($query_isbnLibro) > 0) {
            $record = mysql_result($query_isbnLibro, 0, "isbn");
            return $record;
        } else
            return 0;
    }

    private function ricercaLibroDaISBN($isbn) { //ritorna 1 se trova gia il libro nel catalogo generale
        $query = mysql_query("select isbn from libro where '{$isbn}' = isbn");
        if (!$query) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        $found = mysql_num_rows($query);
        return $found;
    }

}

?>
