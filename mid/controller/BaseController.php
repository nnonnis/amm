<?php
include_once basename(__DIR__) . '/../view/PageStruct.php';
include_once basename(__DIR__) . '/../model/Data_manager.php';
include_once basename(__DIR__) . '/../view/connessione.php';
include_once basename(__DIR__) . '/../model/Settings.php';

class BaseController {

    public function __construct() {
        
    }

    public static $percOpenBooks = 5; //percentuale di guadagno dell'amministratore del sito OpenBooks (5 percento) su singola vendita.

    public function handleInput(&$request, &$session) {

        $browser = new PageStruct(); //creo la struttura della pagina web

        $browser->setPagina($request['page']); //imposto la pagina richiesta

        if (isset($request["cmd"])) { //se è stato richiamato un comando, allora

            switch ($request["cmd"]) { //controllo quale comando è stato selezionato


                case 'login': {
                        $username = isset($request['username']) ? $request['username'] : ''; //nel caso il nome utente non sia impostato, lascio il campo vuoto
                        $password = isset($request['password']) ? $request['password'] : ''; //idem
                        $this->login($browser, $username, $password); //richiamo la funzione login

                        if ($this->loggedIn()) { //se loggato
                            $user = $_SESSION['user'];
                        }
                        break;
                    }

                case 'logout': {
                        $this->logout($browser);
                        break;
                    }

                default : $this->showLoginPage($browser);
            }
        } else {

            if ($this->loggedIn()) {

                $this->showHomeUtente($browser);
            } else {

                $this->showLoginPage($browser);
            }
        }

        require basename(__DIR__) . '/../view/master.php'; //carico la pagina che funge da "contenitore" delle varie sezioni caricate
    }

    public function &getSessione() { //ritorna sessione
        return $_SESSION;
    }

    public function loggedIn() { //ritorna 1 oppure 0 a seconda che l'utente sia loggato o meno
        return isset($_SESSION) && array_key_exists('user', $_SESSION);
    }

    protected function showHome($browser) { //visualizza home page

        $browser->setTitolo("OpenBooks - Home");
        $browser->setInfo('../mid/view/login/infoUtente.php');
        $browser->setMenu('../mid/view/home/menu.php');
        $browser->setLogo('../mid/view/home/logo.php');
        $browser->setSidebar('../mid/view/home/sidebar.php');
        $browser->setContent('../mid/view/home/content.php');
        $browser->setFooter('../mid/view/home/footer.php');
    }

    protected function showLoginPage($browser) {

        $browser->setTitolo("OpenBooks - Login page");
        $browser->setInfo('../mid/view/login/infoUtente.php');
        $browser->setMenu('../mid/view/login/menu.php');
        $browser->setLogo('../mid/view/login/logo.php');
        $browser->setSidebar('../mid/view/login/sidebar.php');
        $browser->setContent('../mid/view/login/login-content.php');
        $browser->setFooter('../mid/view/login/footer.php');
    }

    protected function showHomeCliente($browser) {

        $browser->setTitolo("OpenBooks - Cliente");
        $browser->setInfo('../mid/view/login/infoUtente.php');
        $browser->setMenu('../mid/view/cliente/menu.php');
        $browser->setLogo('../mid/view/cliente/logo.php');
        $browser->setSidebar('../mid/view/cliente/sidebar.php');
        $browser->setContent('../mid/view/cliente/content.php');
        $browser->setFooter('../mid/view/cliente/footer.php');
    }

    protected function showHomeVenditore($browser) {

        $browser->setTitolo("OpenBooks - Gestione vendite");
        $browser->setInfo('../mid/view/login/infoUtente.php');
        $browser->setMenu('../mid/view/venditore/menu.php');
        $browser->setLogo('../mid/view/venditore/logo.php');
        $browser->setSidebar('../mid/view/venditore/sidebar.php');
        $browser->setContent('../mid/view/venditore/content.php');
        $browser->setFooter('../mid/view/venditore/footer.php');
    }

    protected function showHomeAdmin($browser) { //non utilizzato nel progetto

        $browser->setTitolo("OpenBooks - Amministratore");
        $browser->setInfo('../mid/view/login/infoUtente.php');
        $browser->setMenu('../mid/view/admin/menu.php');
        $browser->setLogo('../mid/view/admin/logo.php');
        $browser->setSidebar('../mid/view/admin/sidebar.php');
        $browser->setContent('../mid/view/admin/content.php');
        $browser->setFooter('../mid/view/admin/footer.php');
    }

    protected function showHomeUtente($browser) { //sceglie quale home mostrare all'utente
        $user = $_SESSION['tipo']; //contiene l'identificativo della tipologia di utente ( 1 -> amministratore 2->vendit. 3-> cliente)
        switch ($user) {
            case '3':
                $this->showHomeCliente($browser);
                break;

            case '2':
                $this->showHomeVenditore($browser);
                break;

            case '1':
                $this->showHomeAdmin($browser);
                break;
        }
    }

    protected function modificaPassword($id, $new_password) { //modifica la password dato l'id utente e la nuova password
        $risultato = mysql_query("UPDATE utenti SET pass='{$new_password}' WHERE `id`='{$id}';");

        if (!$risultato) {
            echo 'Impossibile aggiornare la passowrd: ' . mysql_error();
            exit;
        }
        return 1;
    }

    protected function modificaDomicilio($id, $new_citta, $new_via, $new_cap, $new_numero, $new_telefono, $new_email) { //mod domicilio
        
        $risultato = mysql_query("UPDATE domicilio SET citta='{$new_citta}', via='{$new_via}', cap='{$new_cap}', "
        . "numero='{$new_numero}', telefono='{$new_telefono}', email='{$new_email}' WHERE utenti_id='{$id}'");

        if (!$risultato) {
            echo 'Impossibile aggiornare il domicilio: ' . mysql_error();
            exit;
        }
        return 1;
    }

    protected function login($browser, $username, $password) { //effettua login con le credenziali in parametro
        $risultato = mysql_query("SELECT id, username, pass, tipo FROM utenti WHERE username = '{$username}' AND pass = '{$password}'");

        if (!$risultato) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        $corrispondenza = mysql_num_rows($risultato);
// utente autenticato

        if ($corrispondenza == 1) {
            $row = mysql_fetch_assoc($risultato);
            $_SESSION['user'] = $row;
            $_SESSION['id'] = $row['id']; //contiene id utente
            $_SESSION['tipo'] = $row['tipo']; //ovvero 1, 2 o 3
            $this->showHomeUtente($browser);
        } else {
            $browser->setMessaggioErrore("Utente sconosciuto o password errata");
            $this->showLoginPage($browser);
        }
    }

    protected function logout($browser) { //effettua logout

        $_SESSION = array(); //azzera sessione

        if (session_id() != '' || isset($_COOKIE[session_name()])) {

            setcookie(session_name(), '', time() - 2592000, '/'); //fa scadere cookie
        }

        session_destroy(); //distrugge sessione corrente
        $this->showLoginPage($browser);
    }

    public function getCreditoVen($user) { //stampa il credito del venditore
        $risultato = mysql_query("SELECT plafond, idVenditore FROM venditori, utenti WHERE username = '{$user['username']}' and utenti_id=id");
        if (!$risultato) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        $record1 = mysql_result($risultato, 0, "plafond");
        return $record1;
    }

    public function setCreditoVen($user, $credito) { //imposta il credito del venditore
        $risultato = mysql_query("UPDATE venditori,utenti SET plafond='{$credito}' WHERE username = '{$user['username']}' and utenti_id=id;");
        if (!$risultato) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        return 1;
    }
    
      public function setCreditoCli($user, $credito) { //imposta il credito del cliente
        $risultato = mysql_query("UPDATE clienti,utenti SET plafond='{$credito}' WHERE username = '{$user['username']}' and utenti_id=id;");
        if (!$risultato) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        return 1;
    }
    
        public function getCreditoCli($user) { //stampa il credito del cliente
        $risultato = mysql_query("SELECT plafond FROM clienti, utenti WHERE username = '{$user['username']}' and utenti_id=id");
        if (!$risultato) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        $record1 = mysql_result($risultato, 0, "plafond");
        return $record1;
    }

    public final static function getPercentualeSottratta() { //restituisce la percentuale di guadagno del sito su ogni singola transazione.
        return BaseController::percOpenBooks;
    }

    public function getQuantita($idVen, $idLib) { //restituisce la quantita di un determinato libro, posseduto da un venditore, dato l'id venditore e id libro
        $query_quantita = mysql_query("select quantita from depositi "
                . "where libro_idLibro='{$idLib}'"
                . " and venditori_idVenditore ='{$idVen}'");
        if (!$query_quantita) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        if (mysql_num_rows($query_quantita) > 0) {
            $record = mysql_result($query_quantita, 0, "quantita");
            return $record;
        } else
            return 0;
    }

}

?>
