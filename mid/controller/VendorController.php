<?php

include_once basename(__DIR__) . '/../view/PageStruct.php';
include_once basename(__DIR__) . '/../model/Data_manager.php';
include_once basename(__DIR__) . '/../view/connessione.php';
include_once basename(__DIR__) . '/../controller/BaseController.php';/** in prova */
include_once basename(__DIR__) . '/../model/Settings.php';

class VendorController extends BaseController {

    public function __construct() {
        
    }

    public function handleInput(&$request, &$session) {

        $browser = new PageStruct();

        $browser->setPagina($request['page']);

        if (!$this->loggedIn()) { //utente non loggato
            $this->showLoginPage($browser);
        } else { //utente loggato
            $user = $_SESSION['user']; 

            if (isset($request['subpage'])) { //è stata richiesta una pagina
                switch ($request['subpage']) {

                    case 'anagrafica':
                        $browser->setSottoPagina('anagrafica');
                        break;
                    case 'ges_mag': //gestione magazzino
                        $browser->setSottoPagina('ges_mag');
                        break;

                    case 'pro_off': //promuovi offerta
                        $browser->setSottoPagina('pro_off');
                        break;

                    case 'cro_tra'://cronologia transazioni
                        $browser->setSottoPagina('cro_tra');
                        break;

                    case 'ric_lib': //ricerca libri
                        $browser->setSottoPagina('ric_lib');
                        break;

                    case 'ric_lib1': //ricerca libri -fase 2
                        $browser->setSottoPagina('ric_lib1');
                        break;

                    case 'ges_pla': //gestione plafond
                        $browser->setSottoPagina('ges_pla');
                        break;

                    case 'ges_pla1': //gestione plafond fase 2
                        $browser->setSottoPagina('ges_pla1');
                        break;

                    case 'edit_books': //modifica libro
                        $browser->setSottoPagina('edit_books');
                        break;

                    case 'add_books': //aggiungi libro (generale)
                        $browser->setSottoPagina('add_books');
                        break;

                    case 'add_books1': //aggiungi libro -fase 2
                        $browser->setSottoPagina('add_books1');
                        break;

                    case 'add_books2': //aggiungi libro (dati parziali) fase 3
                        $browser->setSottoPagina('add_books2');
                        break;

                    default :
                        $browser->setSottoPagina('not-found-content');
                        break;
                }
            }


            $query_idVen = mysql_query("select idVenditore from venditori where utenti_id = '{$_SESSION['id']}'"); //ottengo id venditore
            if (!$query_idVen) { //in caso di errore nel risultato della query
                echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
                exit;
            }
            $idVenditore = mysql_result($query_idVen, 0,"idVenditore");

            Data_manager::setIdVenditore($idVenditore); //carico id venditore nel modello data_manager
            Data_manager::setIdUtente($_SESSION['id']); //carico id utente nel modello data_manager

            if (isset($request['cmd'])) { //se il comando è un numero, abilita l'eliminazione di un libro, la cui posizione nel db, corrisponde a quel numero
                $tmp = is_numeric($request['cmd']); //restituisce 1 se cmd è un numero
                if ($tmp = 1) //è un numero
                    $this->cancellaLibroDaDeposito($request['cmd'], $user); //funzione che cancella il libro dal deposito nella posizione cmd.


                switch ($request['cmd']) { //è stato chiamato un comando

                case 'mod_pass': //modifica password
                    $new_password = $request['password'];
                    BaseController::modificaPassword($_SESSION['id'], $new_password); //richiamo la funzione per la modifica
                    break;

                case 'mod_domicilio': //modifica domicilio
                    $new_citta = $request['citta'];
                    $new_via = $request['via'];
                    $new_cap = $request['cap'];
                    $new_numero = $request['numero'];
                    $new_telefono = $request['telefono'];
                    $new_email = $request['email'];
                    BaseController::modificaDomicilio($_SESSION['id'], $new_citta,$new_via,$new_cap,$new_numero,$new_telefono,$new_email);
                    break;

                    case 'add_books': //nuovo libro in deposito
                        $isbn = isset($request['isbn']) ? $request['isbn'] : ''; //controllo codice isbn
                        $_SESSION['isbn'] = $isbn; //e lo carico in sessione

                        $found = $this->ricercaLibroDaISBN($isbn); //restituisce 1 se il libro è gia catalogato, 0 altrimenti

                        if ($found) { //se il codice isbn è già catalogato (libro gia catalogato) allora
                            $browser->setSottoPagina('add_books2'); //passa allo step 2, dove verrà inserita solo la quantità da caricare e il prezzo
                        } else {
//procedura registrazione nuovo libro completa
                            $browser->setSottoPagina('add_books1'); //registriamo tutte le informazioni del libro, come isbn, autore, genere ecc.
                        }
                        break;



                case 'add_books1':  //dobbiamo registrare tutte le informazioni del libro, in quanto non ancora catalogato da nessun venditore.

                        if ($request['titolo']) {
// echo "titolo settato: -->" . $request['titolo'];
                            $titolo = $request['titolo'];
                        } else {
                            $browser->setMessaggioErrore("Per completare la catalogazione devi inserire il titolo.");
                            $browser->setSottoPagina('add_books1');
                        }
                        if ($request['autore1']) {
                            $autore1 = $request['autore1'];
                        } else {
                            $browser->setMessaggioErrore("Per completare la catalogazione devi inserire almeno un autore sul campo Autore1.");
                            $browser->setSottoPagina('add_books1');
                        }
                        $autore2 = isset($request['autore2']) ? $request['autore2'] : '';
                        $autore3 = isset($request['autore3']) ? $request['autore3'] : '';
                        $genere = $request['genere'];

                        $this->addBookToCatalog($titolo, $autore1, $autore2, $autore3, $genere, $_SESSION['isbn']); //quindi aggiungo il libro al catalogo dei libri
                        $browser->setSottoPagina('add_books2'); //passo allo step 2, ovvero inserire quantita costo, prezzo ecc..
                        
                        break;




                case 'store': //ordinazione libri all'ingrosso, per venditori.
                        $flag = $this->addBookToDeposit($request, $browser); //la funzione può restituire un codice errore (flag)
                        if ($flag == 0) {
                            $browser->setMessaggioErrore("Impossibile procedere con l&apos; ordinazione. Credito insufficiente.");
                        } else
                        if ($flag == 1) {
                            $browser->setMessaggioErrore("Per completare la catalogazione devi inserire la quantita da acquistare.");
                            $browser->setSottoPagina('add_books2');
                        } else
                        if ($flag == 2) {
                            $browser->setMessaggioErrore("Per completare la catalogazione devi inserire il costo del libro.");
                            $browser->setSottoPagina('add_books2');
                        } else
                        if ($flag == 3) {
                            $browser->setMessaggioErrore("Per completare la catalogazione devi inserire il prezzo di vendita.");
                            $browser->setSottoPagina('add_books2');
                        } else
                            $browser->setSottoPagina('ges_mag');
                        break;




                case 'edit_submit': //conferma modifiche ai dati relativi ad un libro in deposito
                        $prezzo = $request['prezzo'];
                        $quantita = $request['quantita'];
                        $idLibro = $_SESSION['idLib'];
                        mysql_query("UPDATE depositi SET prezzo='{$prezzo}', quantita='{$quantita}' "
                                . "WHERE venditori_idVenditore='{$idVenditore}' AND libro_idLibro='{$idLibro}'");
                        $browser->setSottoPagina('ges_mag');
                        break;

                case 'pro_off': //promuovi offerta
                        $this->setSconto($request['idDep'], $request['sconto']); //imposto lo sconto relativo ad un libro identificato dall'id del deposito (univoco)
                        $browser->setSottoPagina('ges_mag');
                        break;
                }
            }
        }
        $this->showHomeUtente($browser);
        require basename(__DIR__) . '/../view/master.php';
    }

///////FUNZIONI ///////

    private function prelevaDenaro($importo) {
        $credito = BaseController::getCreditoVen($_SESSION['user']);
        if ($importo < $credito) { //faccio prelevare solo se l'importo da prelevare è inferiore al credito di cui dispone l'utente
            if ($importo < 0) { //evito un bug, ovvero il prelevare un "importo negativo", che corrisponderebbe ad accreditare soldi in modo illecito nell'account.
                echo "Non puoi inserire un importo negativo.";
                return;
            }
            $credito = $credito - $importo;
            BaseController::setCreditoVen($_SESSION['user'], $credito); //imposto il nuovo credito, una volta fatto il prelievo.
            echo "prelievo avvenuto con successo: nuovo credito &euro; " . BaseController::getCreditoVen($_SESSION['user']);
        }
    }

    private function ricercaPresenzaDaTitolo($titolo) { //restituisce la query del libro, dato il titolo
        $idVenditore = Data_manager::getIdVenditore();
        $query_search = mysql_query("select titolo, quantita from libro, depositi where titolo = '{$titolo}' "
                . "and libro.idLibro = depositi.libro_idLibro and venditori_idVenditore = '{$idVenditore}'");
        if (!$query_search) {
            echo 'Impossibile eseguire la ricerca del libro nel db: ' . mysql_error();
            exit;
        }
        if (mysql_num_rows($query_search) > 0) {
            return $query_search;
        } else
            return 0;
    }

    private function ricercaPresenzaDaIsbn($isbn) { //restituisce la query del libro dato l'isbn
        $idVenditore = Data_manager::getIdVenditore();
        $query_search = mysql_query("select titolo, quantita from depositi, libro where isbn = '{$isbn}' "
                . "and libro.idLibro = depositi.libro_idLibro and venditori_idVenditore = '{$idVenditore}'");
        if (!$query_search) {
            echo 'Impossibile eseguire la ricerca del libro nel db: ' . mysql_error();
            exit;
        }
        if (mysql_num_rows($query_search) > 0) {
            return $query_search;
        } else
            return 0;
    }

    private function ricercaPresenzaDaPrezzo($flag, $euro) { //restituisce la query con cui ottenere l'id del libro.
        //La query in questa funzione infatti può cambiare a seconda del radio button che è stato selezionato. L'id del libro
        //verrà calcolato successivamente(non in questa funzione).
        //
//flag = 1 -> Maggiore di €..
//flag = 2 -> Minore di €..
//flag = 3 -> Uguale a €..
        $idVenditore = Data_manager::getIdVenditore();
        $query_search = "";
        if ($flag == 1) { //se il radio button era quello relativo a "maggiore di €.."
            $query_search = mysql_query("select titolo, quantita from libro,depositi where prezzo > '{$euro}' "
                    . "and venditori_idVenditore = '{$idVenditore}' and depositi.libro_idLibro=libro.idLibro");
            if (!$query_search) {
                echo 'Impossibile eseguire la ricerca del libro nel db: ' . mysql_error();
                exit;
            }
        }
        if ($flag == 2) { //se il radio button era quello relativo a "minore di €.."
            $query_search = mysql_query("select titolo, quantita from libro,depositi where prezzo < '{$euro}' "
                    . "and venditori_idVenditore = '{$idVenditore}' and depositi.libro_idLibro=libro.idLibro");
            if (!$query_search) {
                echo 'Impossibile eseguire la ricerca del libro nel db: ' . mysql_error();
                exit;
            }
        }
        if ($flag == 3) { //se il radio button era quello relativo a "uguale ad €.."
            $query_search = mysql_query("select titolo, quantita from libro,depositi where prezzo = '{$euro}' "
                    . "and venditori_idVenditore = '{$idVenditore}' and depositi.libro_idLibro=libro.idLibro");
            if (!$query_search) {
                echo 'Impossibile eseguire la ricerca del libro nel db: ' . mysql_error();
                exit;
            }
        }
        if (mysql_num_rows($query_search) > 0) {
            return $query_search; //restituisco la query (se c'è un potenziale risultato)
        } else
            return 0; //restituisco zero altrimenti.
    }

    private function setSconto($idDep, $sconto) { //imposta lo sconto su un libro e solo su un venditore.
        $query_setSconto = mysql_query("UPDATE depositi SET sconto='{$sconto}' WHERE idDeposito='{$idDep}'");
        if (!$query_setSconto) {
            echo 'Impossibile aggiornare il record: ' . mysql_error();
            exit;
        }
    }

    private function getIdFromIsbn($isbn) { //restituisce id libro dato l'isbn

        $query_idLibro = mysql_query("select idLibro from libro where isbn = '{$isbn}' ");
        if (!$query_idLibro) {
            echo 'Impossibile eseguire la ricerca di idLibro da isbn: ' . mysql_error();
            exit;
        }
        if (mysql_num_rows($query_idLibro) > 0) {
            $record = mysql_result($query_idLibro, 0, "idLibro");
            return $record;
        } else
            return 0;
    }

    private function getIsbnFromId($id) { //restituisce isbn dato l'id del libro
        $query_isbnLibro = mysql_query("select isbn from libro where idLibro = '{$id}' ");
        if (!$query_isbnLibro) {
            echo 'Impossibile eseguire la ricerca di idLibro da isbn: ' . mysql_error();
            exit;
        }
        if (mysql_num_rows($query_isbnLibro) > 0) {
            $record = mysql_result($query_isbnLibro, 0, "isbn");
            return $record;
        } else
            return 0;
    }

    //aggiunge un libro al catalogo generale comune a tutti i venditori:
    private function addBookToCatalog($titolo, $autore1, $autore2, $autore3, $genere, $isbn) { 
        $mysqli = new mysqli();
        $mysqli->connect(Settings::$db_host, Settings::$db_user, Settings::$db_password, Settings::$db_name);
        $mysqli->autocommit(FALSE); //avvio la transazione (anche se in questo caso non servirebbe in quanto la query è solo una..)

        $query = "INSERT INTO libro (titolo, autore1, autore2, autore3, genere, isbn)
            VALUES ('{$titolo}', '{$autore1}', '{$autore2}', '{$autore3}', '{$genere}', '{$isbn}')";

        $result = $mysqli->query($query);
        if ($mysqli->errno > 0) { // c'è stato un errore nell'esecuzione della query
            $mysqli->rollback(); //annullo l'azione fatta dalla query
            error_log("Errore nella esecuzione della query$mysqli->errno: $mysqli->error", 0);
        }
        $mysqli->autocommit(true); //reimposto il commit automatico
    }

    private function addBookToDeposit(&$request) { //aggiunta di un libro al deposito venditore.
        $quantita = 0;
        $costo = 0;
        $prezzo = 0;
        $sconto = 0;
        $credito = 0;

        if ($request['quantita'])
            $quantita = $request['quantita'];
        else
            return 1; //se la quantità non è impostata, il chiamante visualizza un errore, relativo a questo valore di return.

        if ($request['costo'])
            $costo = $request['costo'];
        else
            return 2;

        if ($request['prezzo'])
            $prezzo = $request['prezzo'];
        else
            return 3;

        if ($request['sconto'])
            $sconto = $request['sconto'];
        else
            $sconto = 0;

        $idVen = Data_manager::getIdVenditore(); //registro idVenditore
        $isbn = $_SESSION['isbn'];               //registro isbn che identifica il libro da aggiungere (precedentemente caricato in sessione)
        $idLibro = $this->getIdFromIsbn($isbn); //registro l'id del libro da aggiungere

//controllo se il venditore ha già quel libro nel suo deposito. In caso affermativo, non aggiungo un nuovo record, 
//ma modifico quello già esistente.
        $query_is_present = mysql_query("select idDeposito, quantita "
                . "from depositi "
                . "where venditori_idVenditore='{$idVen}' "
                . "and libro_idLibro='{$idLibro}'");

        if (!$query_is_present) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        $found = mysql_num_rows($query_is_present); //$found conterrà il numero di corrispondenze trovate.
//qui inizia una transazione:
        $mysqli = new mysqli();
        $mysqli->connect(Settings::$db_host, Settings::$db_user, Settings::$db_password, Settings::$db_name);
        $mysqli->autocommit(FALSE);

        if ($found == 1) { //se il libro è gia presente in deposito, allora controllo quanti ne possiede
            $quantita_mag = mysql_result($query_is_present, 0, "quantita"); //var che contiene la quantità posseduta
            $idDeposito = mysql_result($query_is_present, 0, "idDeposito"); //memorizzo l'id deposito, che mi servirà dopo nella query per l'aggiornamento della quantità

            if ($mysqli->connect_errno != 0) { //ci sono stati errori nell'esecuzione della query
                $idErrore = $mysqli->connect_errno;
                $msg = $mysqli->connect_error;
                error_log("Errore nella connessione al server $idErrore: $msg", 0);
                echo"Errore nella connessione $msg";
            } else {
                $credito = BaseController::getCreditoVen($_SESSION['user']);
                $importo = ($costo * $quantita);
                if ($credito < $importo) {
                    $mysqli->autocommit(true);
                    return 0; //in caso di credito insufficiente termino la chiamata e chiudo la transazione.
                } else
//scalo i soldi al venditore, quindi imposto il nuovo plafond.
                    $new_cred = $credito - $importo;
                $query = "UPDATE venditori SET plafond='{$new_cred}' WHERE idVenditore='{$idVen}'";

                $result = $mysqli->query($query);
                if ($mysqli->errno > 0) { // c'è stato un errore nell'esecuzione della query
                    $mysqli->rollback(); //ANNULLO LE AZIONI FATTE DALLA QUERY IN CASO DI ERRORE.
                    error_log("Errore nella esecuzione della query$mysqli->errno: $mysqli->error", 0);
                }
//aggiungo al numero dei nuovi libri, anche la quantità gia presente in deposito.
                $quantita = $quantita + $quantita_mag;
                $query = "UPDATE depositi SET costo='{$costo}', prezzo='{$prezzo}', sconto='{$sconto}', quantita='{$quantita}' "
                        . "WHERE idDeposito='{$idDeposito}'";

                $result = $mysqli->query($query);
                if ($mysqli->errno > 0) { // c'è stato un errore nell'esecuzione della query
                    $mysqli->rollback(); //in caso di errore annullo tutta l'operazione
                    error_log("Errore nella esecuzione della query$mysqli->errno: $mysqli->error", 0);
                }
            }
        } else { //il venditore non ha il libro in deposito, quindi inseriamo un nuovo record
            $query = "INSERT INTO depositi (quantita,costo,prezzo,sconto,libro_idLibro, venditori_idVenditore) "
                    . "VALUES ('{$quantita}', '{$costo}', '{$prezzo}', '{$sconto}', '{$idLibro}', '{$idVen}')";
            $result = $mysqli->query($query);
            if ($mysqli->errno > 0) { // c'è stato un errore nell'esecuzione della query
                $mysqli->rollback(); //annullo tutto
                error_log("Errore nella esecuzione della query$mysqli->errno: $mysqli->error", 0);
            } else { //query eseguita.
                $credito = BaseController::getCreditoVen($_SESSION['user']);
                $importo = ($costo * $quantita);
                if ($credito < $importo) {
                    $mysqli->autocommit(true);
                    return 0; //in caso di credito insufficiente termino la chiamata e chiudo la transazione.
                } else
//scalo i soldi al venditore, quindi imposto il nuovo plafond.
                    $new_cred = $credito - $importo;
                $query = "UPDATE venditori SET plafond='{$new_cred}' WHERE idVenditore='{$idVen}'";

                $result = $mysqli->query($query);
                if ($mysqli->errno > 0) { // c'è stato un errore nell'esecuzione della query
                    $mysqli->rollback(); //annullo tutto
                    error_log("Errore nella esecuzione della query$mysqli->errno: $mysqli->error", 0);
                }
            }
        }
        $mysqli->autocommit(true); //tutte le operazioni hanno avuto esito positivo. Chiudo la transazione.
        return 4; //se il chiamante legge 4 come valore di ritorno, significa che tutto è andato bene
    }

    private function cancellaLibroDaDeposito(&$request, $user) {
        $cod = $request; //contiene codice libro presente in deposito, da eliminare
        $idVen = Data_manager::getIdVenditore(); //contiene id venditore


        $query_delete = mysql_query("delete from depositi where libro_idLibro='{$cod}' and venditori_idVenditore='{$idVen}' ");
        if (!$query_delete) {
            echo 'Impossibile eseguire la rimozione dal db: ' . mysql_error();
            exit;
        }
    }

    private function ricercaLibroDaISBN($isbn) { //ritorna 1 se trova gia il libro nel catalogo generale
        $query = mysql_query("select isbn from libro where '{$isbn}' = isbn");
        if (!$query) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        $found = mysql_num_rows($query);
        return $found;
    }

}

?>
