<?php

//LIBRI 

include_once basename(__DIR__) . '/../view/PageStruct.php';
include_once basename(__DIR__) . '/../model/Utente.php';
//include_once basename(__DIR__) . '/../model/Utenti_data.php';
include_once basename(__DIR__) . '/../view/connessione.php';

class BaseController {

    public function __construct() {
        
    }

    public function handleInput(&$request, &$session) {

        $browser = new PageStruct();
        //debugging
        //echo '<br /> index.php ti ha passato alla pagina BaseController, con request page ->';
        //echo $request["page"];
        //echo '<br /> e cmd settato a ->';
        //if (isset($request["cmd"])) {
        //    echo $request["cmd"];
       // } else {
        //    echo ' non settato <br />';
        //}
        //
// imposto la pagina
        $browser->setPagina($request['page']);

// imposto il token per impersonare un utente (nel caso lo stia facendo)
        $this->setImpToken($browser, $request);

        if (isset($request["cmd"])) {
// abbiamo ricevuto un comando
            switch ($request["cmd"]) {
                case 'login':

                    $username = isset($request['username']) ? $request['username'] : '';
                    $password = isset($request['password']) ? $request['password'] : '';
                    $this->login($browser, $username, $password);
                    echo (" punto 3 ");
// questa variabile viene poi utilizzata dalla vista
                    if ($this->loggedIn())  //controlla qui
                        echo (" punto 4 ");
                        $user = $_SESSION['username'];//controlla qui
                    break;
                default : $this->showLoginPage();
            }
        } else {
            echo " punto 1 ";
            if ($this->loggedIn()) {//controlla qui
            echo " punto 2 ";
//utente autenticato
// questa variabile viene poi utilizzata dalla vista
                $user = $_SESSION['username'];//controlla qui

                $this->showHomeUtente($browser);
            } else {

                $this->showLoginPage($browser);
            }
        }

        require basename(__DIR__) . '/../view/master.php';
    }

    public function &getSessione() {
        return $_SESSION;
    }

    /**
     * Verifica se l'utente sia correttamente autenticato
     * @return boolean true se l'utente era gia' autenticato, false altrimenti
     */
    protected function loggedIn() {
        echo ($_SESSION['auth']);
        echo ("<br />");
        $a = array_key_exists("logged", $_SESSION);
        echo ($a);
                if (isset($_SESSION) && array_key_exists('logged', $_SESSION)) echo (" esiste ");
                else echo " non esiste ";
        return isset($_SESSION) && array_key_exists('logged', $_SESSION); //controlla qui
    }

    /**
     * Imposta la vista master.php per visualizzare la pagina di login
     * @param ViewDescriptor $vd il descrittore della vista
     */
    protected function showLoginPage($browser) {
// mostro la pagina di login
        $browser->setTitolo("OpenBooks - Login page");
        $browser->setMenu('../mid/view/login/menu.php');
        $browser->setLogo('../mid/view/login/logo.php');
        $browser->setSidebar('../mid/view/login/sidebar.php');
        $browser->setContent('../mid/view/login/login-content.php');
        $browser->setFooter('../mid/view/login/footer.php');
    }

    /**
     * Imposta la vista master.php per visualizzare la pagina di gestione
     * dello studente
     * @param ViewDescriptor $vd il descrittore della vista
     */
    protected function showHomeCliente($browser) {
// mostro la home degli studenti
        $browser->setTitolo("OpenBooks - Cliente");
        $browser->setMenu('/../view/cliente/menu.php');
        $browser->setLogo('/../view/cliente/logo.php');
        $browser->setSidebar('/../view/cliente/sidebar.php');
        $browser->setContent('/../view/cliente/content.php');
        $browser->setFooter('/../view/cliente/footer.php');
    }

    /**
     * Imposta la vista master.php per visualizzare la pagina di gestione
     * del docente
     * @param ViewDescriptor $vd il descrittore della vista
     */
    protected function showHomeVenditore($browser) {
// mostro la home dei docenti
        $browser->setTitolo("OpenBooks - Gestione vendite");
        $browser->setMenu('/../view/venditore/menu.php');
        $browser->setLogo('/../view/venditore/logo.php');
        $browser->setSidebar('/../view/venditore/sidebar.php');
        $browser->setContent('/../view/docente/content.php');
        $browser->setFooter('/../view/docente/footer.php');
    }

    /**
     * Imposta la vista master.php per visualizzare la pagina di gestione
     * dell'amministratore
     * @param ViewDescriptor $vd il descrittore della vista
     */
    protected function showHomeAdmin($browser) {
// mostro la home degli amministratori
        $browser->setTitolo("OpenBooks - Amministratore");
        $browser->setMenu('../mid/view/admin/menu.php');
        $browser->setLogo('../mid/view/admin/logo.php');
        $browser->setSidebar('../mid/view/admin/sidebar.php');
        $browser->setContent('../mid/view/admin/content.php');
        $browser->setFooter('../mid/view/admin/footer.php');
    }

    /**
     * Seleziona quale pagina mostrare in base al ruolo dell'utente corrente
     * @param ViewDescriptor $vd il descrittore della vista
     */
    protected function showHomeUtente($browser) {
        $user = $_SESSION['tipo'];
        switch ($user) {
            case Utente::Cliente:
                $this->showHomeCliente($browser);
                break;

            case Utente::Commerciante:
                $this->showHomeVenditore($browser);
                break;

            case Utente::Amministratore:
                $this->showHomeAdmin($browser);
                break;
        }
    }

    /**
     * Imposta la variabile del descrittore della vista legato 
     * all'utente da impersonare nel caso sia stato specificato nella richiesta
     * @param ViewDescriptor $vd il descrittore della vista
     * @param array $request la richiesta
     */
    protected function setImpToken(PageStruct $browser, &$request) {

        if (array_key_exists('_imp', $request)) {
            $browser->setImpToken($request['_imp']);
        }
    }

    /**
     * Procedura di autenticazione 
     * @param ViewDescriptor $vd descrittore della vista
     * @param string $username lo username specificato
     * @param string $password la password specificata
     */
    protected function login($browser, $username, $password) {
// carichiamo i dati dell'utente
        $risultato = mysql_query("SELECT ID, username, tipo FROM utenti WHERE username = '{$username}' AND pass = '{$password}'");
        if (!$risultato) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        $corrispondenza = mysql_num_rows($risultato);
// utente autenticato
        if ($corrispondenza == 1) {
            $_SESSION['ID'] = $risultato['ID'];
            $_SESSION['username'] = $risultato['username'];
            $_SESSION['tipo'] = $risultato['tipo'];
            $_SESSION['auth'] = 'logged'; //da controllare
            $this->showHomeUtente($browser);
        } else {
            $browser->setMessaggioErrore("Utente sconosciuto o password errata");
            $this->showLoginPage($browser);
        }
    }

    /**
     * Procedura di logout dal sistema 
     * @param type $vd il descrittore della pagina
     */
    protected function logout($browser) {
// reset array $_SESSION
        $_SESSION = array();
// termino la validita' del cookie di sessione
        if (session_id() != '' || isset($_COOKIE[session_name()])) {
// imposto il termine di validita' al mese scorso
            setcookie(session_name(), '', time() - 2592000, '/');
        }
// distruggo il file di sessione
        session_destroy();
        $this->showLoginPage($browser);
    }

    /**
     * Aggiorno l'indirizzo di un utente (comune a Studente e Docente)
     * @param User $user l'utente da aggiornare
     * @param array $request la richiesta http da gestire
     * @param array $msg riferimento ad un array da riempire con eventuali
     * messaggi d'errore
     */
    protected function aggiornaIndirizzo($user, &$request, &$msg) {

        if (isset($request['via'])) {
            if (!$user->setVia($request['via'])) {
                $msg[] = '<li>La via specificata non &egrave; corretta</li>';
            }
        }
        if (isset($request['civico'])) {
            if (!$user->setNumeroCivico($request['civico'])) {
                $msg[] = '<li>Il formato del numero civico non &egrave; corretto</li>';
            }
        }
        if (isset($request['citta'])) {
            if (!$user->setCitta($request['citta'])) {
                $msg[] = '<li>La citt&agrave; specificata non &egrave; corretta</li>';
            }
        }
        if (isset($request['provincia'])) {
            if (!$user->setProvincia($request['provincia'])) {
                $msg[] = '<li>La provincia specificata &egrave; corretta</li>';
            }
        }
        if (isset($request['cap'])) {
            if (!$user->setCap($request['cap'])) {
                $msg[] = '<li>Il CAP specificato non &egrave; corretto</li>';
            }
        }
    }

    /**
     * Aggiorno l'indirizzo email di un utente (comune a Studente e Docente)
     * @param User $user l'utente da aggiornare
     * @param array $request la richiesta http da gestire
     * @param array $msg riferimento ad un array da riempire con eventuali
     * messaggi d'errore
     */
    protected function aggiornaEmail($user, &$request, &$msg) {
        if (isset($request['email'])) {
            if (!$user->setEmail($request['email'])) {
                $msg[] = '<li>L\'indirizzo email specificato non &egrave; corretto</li>';
            }
        }
    }

    /**
     * Aggiorno la password di un utente (comune a Studente e Docente)
     * @param User $user l'utente da aggiornare
     * @param array $request la richiesta http da gestire
     * @param array $msg riferimento ad un array da riempire con eventuali
     * messaggi d'errore
     */
    protected function aggiornaPassword($user, &$request, &$msg) {
        if (isset($request['pass1']) && isset($request['pass2'])) {
            if ($request['pass1'] == $request['pass2']) {
                if (!$user->setPassword($request['pass1'])) {
                    $msg[] = '<li>Il formato della password non &egrave; corretto</li>';
                }
            } else {
                $msg[] = '<li>Le due password non coincidono</li>';
            }
        }
    }

    /**
     * Crea un messaggio di feedback per l'utente 
     * @param array $msg lista di messaggi di errore
     * @param ViewDescriptor $vd il descrittore della pagina
     * @param string $okMsg il messaggio da mostrare nel caso non ci siano errori
     */
    protected function creaFeedbackUtente(&$msg, $browser, $okMsg) {
        if (count($msg) > 0) {
// ci sono messaggi di errore nell'array,
// qualcosa e' andato storto...
            $error = "Si sono verificati i seguenti errori \n<ul>\n";
            foreach ($msg as $m) {
                $error = $error . $m . "\n";
            }
// imposto il messaggio di errore
            $browser->setMessaggioErrore($error);
        } else {
// non ci sono messaggi di errore, la procedura e' andata
// quindi a buon fine, mostro un messaggio di conferma
            $browser->setMessaggioConferma($okMsg);
        }
    }

}

?>
