<h2>Utenza amministratore</h2>
<p>Operazioni disponibili:</p>
<ul>
    <li>Gestisci/impersona utenti</li>
    <li>Consulta transazioni</li>
    <li>Modifica parametri di guadagno</li>
    <li>Consulta introiti</li>
    <li>Elimina libro dal database</li>
</ul>
