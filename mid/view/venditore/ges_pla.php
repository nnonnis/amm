<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
include_once basename(__DIR__) . '/../controller/BaseController.php';
include_once basename(__DIR__) . '/../view/connessione.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gestione plafond</title>
    </head>
    <body>

        <p>Da questa pagina puoi consultare il tuo credito.</br>
            Quando vendi un libro, una percentuale del ricavato &egrave; devoluta automaticamente ad OpenBooks.</br>
            Puoi controllare la percentuale devoluta in questa pagina. </br>
            Puoi prelevare denaro tramite PayPal o versamento bancario.</br>
            </hr>
        </p>
            <hr/>
            <p> Credito disponibile: <h2> &euro;
                <?php echo BaseController::getCreditoVen($user); ?></h2>
            <hr/>
            <div id="form_payment_left">
                <p>Trasferisci su conto corrente IBAN</p>
                <form method="post" action="index.php?page=venditore">
                    <input type="hidden" name="subpage" value="ges_pla1">
                    <label for="importo">Importo &euro; </label>
                    <input type="text" name="transfert" id="transfert_iban" value='0.00'>
                    <div class="submit">
                        <button type="submit">
                            <img src="../immagini/preleva.png" class="button_ok"/> Preleva
                        </button>
                    </div>
                </form>
            </div>
            <div id="form_payment_center">
                <p>Preleva con PayPal</p>
                <form method="post" action="index.php?page=venditore">
                    <input type="hidden" name="subpage" value="ges_pla1"/>
                    <label for="titolo">Importo &euro; </label>
                    <input type="text" name="transfert" id="paypal" value='0.00'/>
                    <div class="submit">
                        <button type="submit">
                            <img src="../immagini/preleva.png" class="button_ok"/> Preleva
                        </button>
                    </div>
                </form>
            </div>
            <div id="form_payment_right">
                <p>Ricarica plafond</p>
                <form method="post" action="index.php?page=venditore">
                    <input type="hidden" name="subpage" value="ges_pla1"/>
                    <label for="titolo">Importo: &euro; </label>
                    <input type="text" name="ricarica" id="ricarica" value='0.00'/>
                    <div class="submit">
                        <button type="submit">
                            <img src="../immagini/preleva.png" class="button_ok"/> Ricarica
                        </button>
                    </div>
                </form>
            </div>
            <p>Percentuale destinata ad Openbooks S.p.A</p>
            <h2>- 5% -</h2>
    </body>
</html>
