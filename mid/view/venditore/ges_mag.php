<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gestione magazzino</title>
    </head>
    <body>


        <table border="0" cellspacing="2" cellpadding="1">   
            <tr>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <form method="post" action="index.php?page=venditore">
                        <button name=”add” type=”submit”>
                            <img src="../immagini/add_books.png" class="button_norm">Aggiungi libri
                            <input type="hidden" name="subpage" value="add_books"/>
                        </button> 
                    </form>
                    </font></td>
            </tr>
        </table>

        <?php
        if (BaseController::loggedIn()) {

            $idVenditore = Data_manager::getIdVenditore();
            $query_records_dep = mysql_query("select libro_idLibro, costo, prezzo, sconto, quantita "
                    . "from depositi where venditori_idVenditore = '{$idVenditore}'"); //carica tutti i libri venduti dal venditore corrente
            if (!$query_records_dep) {
                echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
                exit;
            }
            $numero_libri = mysql_num_rows($query_records_dep); //ottengo numero libri del venditore selezionato


            $query_titles_books = mysql_query("select titolo, isbn from libro, depositi where venditori_idVenditore='{$idVenditore}' "
                    . "and libro_idLibro=idLibro");  //ottengo i nomi dei libri posseduti dal venditore selezionato
            if (!$query_titles_books) {
                echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
                exit;
            }
            ?>
            <table border="1" cellspacing="1" cellpadding="1">
                <thead>
                    <tr>
                        <th><font face="Arial, Helvetica, sans-serif">Modifica</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Elimina</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">ISBN</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">costo</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Prezzo al pubblico</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Sconto (%)</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Quantita'</font></th>
                    </tr>  
                </thead>
                <?php
                $i = 0;
                while ($i < $numero_libri) {
                    $cod = mysql_result($query_records_dep, $i, "libro_idLibro");
                    $is = mysql_result($query_titles_books, $i, "isbn");
                    $ti = mysql_result($query_titles_books, $i, "titolo");
                    $co = mysql_result($query_records_dep, $i, "costo");
                    $pr = mysql_result($query_records_dep, $i, "prezzo");
                    $sc = mysql_result($query_records_dep, $i, "sconto");
                    $qt = mysql_result($query_records_dep, $i, "quantita");
                    ?>
                    <tr>

                        <td align="center">  <form method="post" action="index.php?page=venditore">
                                <button name=”edit” type=”submit”>
                                    <img src="../immagini/edit_books.png" class="mini_button">
                                    <input type="hidden" name="subpage" value="edit_books"/>
                                    <input type="hidden" name="edit" value="<?php echo $cod; ?>"/>
                                </button> 
                            </form>
                        </td>

                        <td align="center"> <!-- rimozione libro dal magazzino !-->
                            <form method="post" action="index.php?page=venditore">
                                <button name=”remove” type=”submit”>
                                    <img src="../immagini/remove_books.png" class="mini_button">
                                    <input type="hidden" name="cmd" value="<?php echo $cod; ?>"/>
                                </button> 
                            </form>
                        </td>
                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $is; ?></font></td>
                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $ti; ?></font></td>
                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $co; ?></font></td>
                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $pr; ?></font></td>
                        <td align="center"><font face="Arial, Helvetica, sans-serif">
                            <?php if ($sc > 0) { ?> <div id="sale"> <?php echo $sc;
                } else {
                    echo $sc;
                }
                ?></font></td></div>

                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $qt; ?></font></td>
                    </tr>
                    <?php
                    $i++;
                }
            }
            ?>
        </table>
    </body>
</html>
