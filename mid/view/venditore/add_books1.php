<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <p>Catalogazione nuovo libro:</p>
        <p>Il codice ISBN che hai inserito, non &egrave ancora stato registrato nel nostro database.</br>
            Inserisci i dati relativi al libro che vuoi aggiungere. Questa procedura si verificher&agrave solo una volta.
        </p>

        <div id="form">
            <form method="post" action="index.php?page=venditore">
                <table border="0" cellspacing="3" cellpadding="3">
                    <thead>

                    <input type="hidden" name="cmd" value="add_books1"/>

                    <tr><th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="titolo" id="titolo"/></font></td></tr> 

                    <tr><th><font face="Arial, Helvetica, sans-serif">Autore 1</font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="autore1" id="autore1"/></font></td></tr> 

                    <tr><th><font face="Arial, Helvetica, sans-serif">Autore 2</font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="autore2" id="autore2"/></font></td></tr> 

                    <tr><th><font face="Arial, Helvetica, sans-serif">Autore 3</font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="autore3" id="autore3"/></font></td></tr> 
                    
                    <th><font face="Arial, Helvetica, sans-serif">Genere</font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <select name="genere">
                                <option value="AGRICOLTURA">AGRICOLTURA</option>
                                <option value="AMMINISTRAZIONE">AMMINISTRAZIONE</option>
                                <option value="ANTROPOLOGIA">ANTROPOLOGIA</option>
                                <option value="ARCHEOLOGIA">ARCHEOLOGIA</option>
                                <option value="ARCHITETTURA">ARCHITETTURA</option>
                                <option value="ARTE">ARTE</option>
                                <option value="ASTROLOGIA">ASTROLOGIA</option>
                                <option value="BIBLIOGRAFIA">BIBLIOGRAFIA</option>
                                <option value="BIOGRAFIA">BIOGRAFIA</option>
                                <option value="CHIMICA">CHIMICA</option>
                                <option value="COMMERCIO">COMMERCIO</option>
                                <option value="CUCINA">CUCINA</option>
                                <option value="DIRITTO">DIRITTO</option></option>
                                <option value="DIZIONARI">DIZIONARI</option>
                                <option value="ECONOMIA">ECONOMIA</option>
                                <option value="ECONOMIA_COMMERCIO">ECONOMIA COMMERCIO</option>
                                <option value="EDILIZIA">EDILIZIA</option>
                                <option value="EDUCAZIONE">EDUCAZIONE</option>
                                <option value="ETICA">ETICA</option>
                                <option value="FANTASY">FANTASY</option>
                                <option value="FILOSOFIA">FILOSOFIA</option></option>
                                <option value="FISICA">FISICA</option>
                                <option value="FOTOGRAFIA">FOTOGRAFIA</option>
                                <option value="FUMETTI">FUMETTI</option>
                                <option value="GEOGRAFIA">GEOGRAFIA</option>
                                <option value="GIALLI">GIALLI</option>
                                <option value="GIORNALISMO_EDITORIA">GIORNALISMO EDITORIA</option>
                                <option value="HOBBY">HOBBY</option>
                                <option value="INFORMATICA">INFORMATICA</option>
                                <option value="INGEGNERIA">INGEGNERIA</option>
                                <option value="LETTERATURA">LETTERATURA</option></option>
                                <option value="LETTERATURA_CLASSICA">LETTERATURA CLASSICA</option>
                                <option value="LETTERATURA_ITALIANA">LETTERATURA ITALIANA</option>
                                <option value="LETTERATURA_STRANIERA">LETTERATURA STRANIERA</option>
                                <option value="LIBRI_RARI">LIBRI RARI</option></option>
                                <option value="LINGUA_FRANCESE">LINGUA FRANCESE</option>
                                <option value="LINGUE_STRANIERE">LINGUE STRANIERE</option>
                                <option value="LINGUISTICA">LINGUISTICA</option>
                                <option value="MATEMATICA">MATEMATICA</option>
                                <option value="MEDICINA">MEDICINA</option>
                                <option value="MULTIMEDIA">MULTIMEDIA</option>
                                <option value="NARRATIVA">NARRATIVA</option>
                                <option value="NARRATIVA_ITALIANA">NARRATIVA ITALIANA</option>
                                <option value="NARRATIVA_STRANIERA">NARRATIVA STRANIERA</option>
                                <option value="PARANORMALE">PARANORMALE</option>
                                <option value="POESIA">POESIA</option>
                                <option value="POLITICA">POLITICA</option>
                                <option value="PSICOLOGIA">PSICOLOGIA</option>
                                <option value="RAGAZZI">RAGAZZI</option>
                                <option value="RELIGIONE">RELIGIONE</option>
                                <option value="SAGGISTICA">SAGGISTICA</option>
                                <option value="SCIENZE">SCIENZE</option>
                                <option value="SOCIETÀ">SOCIETÀ</option>
                                <option value="SOCIOLOGIA">SOCIOLOGIA</option>
                                <option value="SPETTACOLO">SPETTACOLO</option>
                                <option value="SPORT">SPORT</option>
                                <option value="STORIA">STORIA</option>
                                <option value="TECNOLOGIA">TECNOLOGIA</option>
                                <option value="TEOLOGIA">TEOLOGIA</option>
                                <option value="UMORISMO">UMORISMO</option>
                                <option value="VARIA">VARIA</option>
                                <option value="VIAGGI">VIAGGI</option>
                            </select>
                    </thead>
                </table>
                <div class="submit">
                    <button type="submit">
                        <img src="../immagini/button_ok.png" class="button_ok"/> Conferma inserimento
                    </button>
                </div>
            </form>
        </div>
        </br>
        </br>
    </body>
</html>
