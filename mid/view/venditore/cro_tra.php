<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cronologia transazioni</title>
    </head>
    <body>
        <p>Cronologia transazioni</p>
        <?php
        $idVenditore= Data_manager::getIdVenditore();
        $query_records_cronologia = mysql_query("select titolo, carrello.quantita, totale from carrello, depositi, libro  "
                . "where articolo=idDeposito and libro_idLibro=idLibro and venditori_idVenditore='{$idVenditore}'"); //ricerca vendite dato l'id venditore
        if (!$query_records_cronologia) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        $num_records = mysql_num_rows($query_records_cronologia); //numero risultati query
        ?>

        <table border="1" cellspacing="1" cellpadding="1">
            <thead>
                <tr>
                    <th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                    <th><font face="Arial, Helvetica, sans-serif">Quantita' venduta</font></th>
                    <th><font face="Arial, Helvetica, sans-serif">Totale</font></th>
                </tr>  
            </thead>
            <?php
            $i = 0;
            while ($i < $num_records) {
                $ti = mysql_result($query_records_cronologia, $i, "titolo");
                $qt = mysql_result($query_records_cronologia, $i, "quantita");
                $pr = mysql_result($query_records_cronologia, $i, "totale");
                ?>
                <tr>
                    <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $ti; ?></font></td>
                    <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $qt; ?></font></td>
                    <td align="center"><font face="Arial, Helvetica, sans-serif"><h3><?php echo "&euro; ".$pr; ?></h3></font></td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </table>
    </body>
</html>
