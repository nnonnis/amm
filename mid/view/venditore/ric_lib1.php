<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $id = 'null';
        $query_search = 'null';
        $numero_libri = 0;

        if (isset($request['titolo'])) { //è stato richiesto di cercare il libro dato il titolo
            $query_search = VendorController::ricercaPresenzaDaTitolo($request['titolo']);
            if($query_search==0){ $numero_libri=0; }
            else
             $numero_libri= mysql_num_rows($query_search);
        }


        if (isset($request['isbn'])) {//è stato richiesto di cercare il libro dato il codice isbn
            $query_search = VendorController::ricercaPresenzaDaIsbn($request['isbn']);
            if($query_search==0){ $numero_libri=0; }
            else
             $numero_libri= mysql_num_rows($query_search);
        }


        if (isset($request['soglia']) and isset($request['importo'])) {//è stato richiesto di cercare il libro data la soglia in euro
            $query_search = VendorController::ricercaPresenzaDaPrezzo($request['soglia'], $request['importo']);
            if($query_search==0){ $numero_libri=0; }
            else
             $numero_libri= mysql_num_rows($query_search);
        }
        ?>
        <!-- visualizza i risultati -->
        <table border="1" cellspacing="1" cellpadding="1">
            <thead>
                <tr>
                    <th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                    <th><font face="Arial, Helvetica, sans-serif">Quantit&agrave;</font></th>
                </tr>  
            </thead>
            <?php
            $i = 0;
            while ($i < $numero_libri) {
                $titolo = mysql_result($query_search, $i, "titolo");
                $quantita = mysql_result($query_search, $i, "quantita");
                ?>
                <tr>
                    <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $titolo; ?></font></td>
                    <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $quantita; ?></font></td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </table>


    </body>
</html>
