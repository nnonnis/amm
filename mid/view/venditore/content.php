<?php
include_once basename(__DIR__) . '/../controller/BaseController.php';
include_once basename(__DIR__) . '/../view/connessione.php';

switch ($browser->getSottoPagina()) {

        case 'anagrafica': //gestione magazzino
        include 'anagrafica.php';
        break;
    
    case 'ges_mag': //gestione magazzino
        include 'ges_mag.php';
        break;

    case 'pro_off': //promuovi offerta
        include 'pro_off.php';
        break;

    case 'cro_tra': //cronologia transazioni
        include 'cro_tra.php';
        break;

    case 'ric_lib': //ricerca libro
        include 'ric_lib.php';
        break;

    case 'ric_lib1': //ricerca libro step 1
        include 'ric_lib1.php';
        break;

    case 'ges_pla': //gestione plafond
        include 'ges_pla.php';
        break;

    case 'ges_pla1': //gestione plafond step 1
        include 'ges_pla1.php';
        break;

    case 'edit_books': //modifica libro
        include 'edit_books.php';
        break;

    case 'add_books': //aggiungi libro
        include 'add_books.php';
        break;

        case 'add_books1': //aggiungi libro step 1
        include 'add_books1.php';
        break;

    case 'add_books2': //aggiungi libro step 2
        include 'add_books2.php';
        break;
        ?>

    <?php default: ?>
        <!-- Home page del venditore !-->
        Benvenuto, <?php echo $row['nome']; ?> <?php echo $row['cognome']; ?>. <!-- visualizza nome e cognome utente-->

        <div id="notice"><h3>Saldo attuale:  


                <?php
                echo "Euro " . BaseController::getCreditoVen($user); //visualizza credito disponibile
                ?>

            </h3>
        </div>
        <h2> Ti trovi nella pagina di amministrazione del venditore, dalla quale potrai accedere alle funzionalita' messe 
            a disposizione per te.</h2>

        <h2>Ecco come funzionano:</h2>
        <h3>Gestione magazzino: Da qui, potrai accedere all'elenco del carico/scarico merce. Potrai gestire l'acquisto dei libri dai fornitori, 
            oltre ovviamente, a gestire la quantit&agrave; presente gia in magazzino.
            <hr>
            Promuovi offerta: Ti consente di stabilire uno sconto da applicare su un determinato libro, espresso in percentuale.
            <hr>
            Cronologia transazioni: Da qui potrai accedere allo storico delle vendite.
            <hr>
            Ricerca libro: Effettua una ricerca di un determinato libro, tramite ISBN, titolo o range di prezzo.
            <hr>
            Gestisci plafond: Strumento con il quale potrai verificare i tuoi guadagni, prelevare o trasferire denaro.
            <hr>
        </h3>
        <?php
        break;
}
?>

