<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gestione promozioni</title>
    </head>
    <body>
        <p>In questa pagina puoi impostare lo sconto per ciascun libro.</br>Attenzione: Dopo aver impostato lo sconto relativo ad un libro,
        clicca il rispettivo pulsante "Applica".</p>
        <hr>
        <?php
        if (BaseController::loggedIn()) {

            $idVenditore = Data_manager::getIdVenditore();

            $query_records_dep = mysql_query("select idDeposito, libro_idLibro, prezzo, sconto "
                    . "from depositi where venditori_idVenditore = '{$idVenditore}'");
            if (!$query_records_dep) {
                echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
                exit;
            }
            $numero_libri = mysql_num_rows($query_records_dep); //ottengo numero libri del venditore selezionato


            $query_titles_books = mysql_query("select titolo, isbn from libro, depositi where venditori_idVenditore='{$idVenditore}' "
                    . "and libro_idLibro=idLibro");  //ottengo i nomi dei libri posseduti dal venditore selezionato
            if (!$query_titles_books) {
                echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
                exit;
            }
            ?>
            <table border="1" cellspacing="1" cellpadding="1">
                <thead>
                    <tr>
                        <th><font face="Arial, Helvetica, sans-serif">ISBN</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Prezzo al pubblico</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Sconto (%)</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Applica</font></th>
                    </tr>  
                </thead>
                <?php
                $i = 0;
                while ($i < $numero_libri) {
                    $idDep = mysql_result($query_records_dep, $i, "idDeposito");
                    $is = mysql_result($query_titles_books, $i, "isbn");
                    $ti = mysql_result($query_titles_books, $i, "titolo");
                    $pr = mysql_result($query_records_dep, $i, "prezzo");
                    $sc = mysql_result($query_records_dep, $i, "sconto");
                    ?>
                    <tr>
                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $is; ?></font></td>
                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $ti; ?></font></td>
                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $pr; ?></font></td>

                    <form method="post" action="index.php?page=venditore">
                        <!--qui è possibile impostare un eventuale sconto su ciascun libro -->
                        <td align="center"><input type="text" name="sconto" id="sconto" value="<?php echo $sc ?>"/></td>

                        <td align="center">  <button name=”applica” type=”submit”>
                                <img src="../immagini/button_ok.png" class="mini_button">
                                <input type="hidden" name="cmd" value="pro_off"/>
                                <input type="hidden" name="idDep" value="<?php echo $idDep ?>"/>
                            </button>
                        </td></form>  

                </tr>
                <?php
                $i++;
            }
        }
        ?>
    </table>
</body>
</html>

