<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Nuovo libro</title>
    </head>
    <body>
        <?php
        include_once basename(__DIR__) . '/../controller/BaseController.php';
        include_once basename(__DIR__) . '/../model/Data_manager.php';
        include_once basename(__DIR__) . '/../view/connessione.php';

$isbn= $_SESSION['isbn'];

        $query_info_book = mysql_query("select idLibro, titolo, autore1, genere, isbn "
                . "from libro where isbn = '{$isbn}' ");
        if (!$query_info_book) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }

        $idLibro= mysql_result($query_info_book, 0, "idLibro");
        Data_manager::setIdLibro($idLibro); //carica l'id del libro trovato nel datamanager;
        ?>
        <div id= "mini_message">
            Credito disponibile: <?php echo "Euro ".BaseController::getCreditoVen($user) ?>
            <br>

            Quantit&agrave; gi&agrave; in magazzino: 
                <?php echo BaseController::getQuantita(Data_manager::getIdVenditore(),Data_manager::getIdLibro());?>
        </div>


        <table border="1" cellspacing="2" cellpadding="2">
            <tr><th><font face="Arial, Helvetica, sans-serif">ID</font></th>
                <th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                <th><font face="Arial, Helvetica, sans-serif">Autore</font></th>
                <th><font face="Arial, Helvetica, sans-serif">Genere</font></th>
                <th><font face="Arial, Helvetica, sans-serif">ISBN</font></th>
            </tr>
            <tr>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="id" id="id" value="<?php echo mysql_result($query_info_book, 0, "idLibro"); ?>" disabled/></font></td>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="titolo" id="titolo" value="<?php echo mysql_result($query_info_book, 0, "titolo"); ?>" disabled/></font></td>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="autore" id="autore" value="<?php echo mysql_result($query_info_book, 0, "autore1"); ?>" disabled/></font></td>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="genere" id="genere" value="<?php echo mysql_result($query_info_book, 0, "genere"); ?>" disabled/></font></td>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="isbn" id="isbn" value="<?php echo mysql_result($query_info_book, 0, "isbn"); ?>" disabled/></font></td>

            </tr>

        </table>
        <div id="form">
            <form method="post" action="index.php?page=venditore">
                <table border="0" cellspacing="3" cellpadding="3">
                    <thead>

                    <input type="hidden" name="cmd" value="store"/>

                    <tr><th><font face="Arial, Helvetica, sans-serif">Quantit&agrave; da caricare in deposito </font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="quantita" id="quantita"/></font></td></tr> 

                    <tr><th><font face="Arial, Helvetica, sans-serif">Costo </font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="costo" id="costo"/></font></td></tr> 


                    <tr><th><font face="Arial, Helvetica, sans-serif">Prezzo al pubblico </font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="prezzo" id="prezzo"/></font></td></tr>


                    <tr><th><font face="Arial, Helvetica, sans-serif">Sconto </font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="sconto" id="sconto"/></font></td></tr>  
                    </thead>
                </table>
                <div class="submit">
                    <button type="submit">
                        <img src="../immagini/button_ok.png" class="button_ok"/> Conferma
                    </button>
                </div>
            </form>
        </div>
        </br>
        </br>
    </body>
</html>
