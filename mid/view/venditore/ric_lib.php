<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ricerca libro</title>
    </head>
    <body>
        <p>Da questa pagina puoi effettuare le ricerche su libri, secondo diversi criteri.</p>
        <hr>

        <div id="mini_form_left">
            <p>Ricerca per titolo:</p>
            <form method="post" action="index.php?page=venditore">
                <input type="hidden" name="subpage" value="ric_lib1"/>
                <label for="titolo">Titolo</label>
                <input type="text" name="titolo" id="titolo"/>
                <br/>
                <div class="submit">
                    <button type="submit">
                        <img src="../immagini/user-search.png" class="button_ok"/> Ricerca
                    </button>
                </div>
            </form>
        </div>
        <div id="mini_form_center">
            <p>Ricerca per ISBN:</p>
            <form method="post" action="index.php?page=venditore">
                <input type="hidden" name="subpage" value="ric_lib1"/>
                <label for="isbn">ISBN</label>
                <input type="text" name="isbn" id="isbn"/>
                <br/>
                <div class="submit">
                    <button type="submit">
                        <img src="../immagini/user-search.png" class="button_ok"/> Ricerca
                    </button>
                </div>
            </form>
        </div>
        <div id="mini_form_right">
            <p>Ricerca per prezzo:</p>
            <form method="post" action="index.php?page=venditore">
                <input type="hidden" name="subpage" value="ric_lib1"/>
                <input type="radio" name="soglia" value="1">Maggiore
                <input type="radio" name="soglia" value="2">Minore
                <input type="radio" name="soglia" value="3">Uguale
                </br>
                a &euro; <input type="text" name="importo" id="importo"/> 
                <br/>
                <div class="submit">
                    <button type="submit">
                        <img src="../immagini/user-search.png" class="button_ok"/> Ricerca
                    </button>
                </div>
            </form>
        </div>
    </body>
</html>
