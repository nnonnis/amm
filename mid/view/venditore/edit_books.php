<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modifica libro</title>
    </head>
    <body>
        <p>Pagina di modifica informazioni libro</p>
        <?php

        $isbn = VendorController::getIsbnFromId($request['edit']);

        $query_info_book = mysql_query("select idLibro, titolo, autore1, genere, isbn "
                . "from libro where isbn = '{$isbn}' ");
        if (!$query_info_book) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        ?>

        <table border="1" cellspacing="2" cellpadding="2">
            <tr><th><font face="Arial, Helvetica, sans-serif">ID</font></th>
                <th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                <th><font face="Arial, Helvetica, sans-serif">Autore</font></th>
                <th><font face="Arial, Helvetica, sans-serif">Genere</font></th>
                <th><font face="Arial, Helvetica, sans-serif">ISBN</font></th>
            </tr>
            <tr>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="id" id="id" value="<?php echo mysql_result($query_info_book, 0, "idLibro"); ?>" disabled/></font></td>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="titolo" id="titolo" value="<?php echo mysql_result($query_info_book, 0, "titolo"); ?>" disabled/></font></td>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="autore" id="autore" value="<?php echo mysql_result($query_info_book, 0, "autore1"); ?>" disabled/></font></td>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="genere" id="genere" value="<?php echo mysql_result($query_info_book, 0, "genere"); ?>" disabled/></font></td>
                <td align="center"><font face="Arial, Helvetica, sans-serif">
                    <input type="text" name="isbn" id="isbn" value="<?php echo mysql_result($query_info_book, 0, "isbn"); ?>" disabled/></font></td>
            </tr>
        </table>

        <?php
        $idVenditore = Data_manager::getIdVenditore();
        $idLibro = $request['edit']; //carica l'id del libro i cui dati sono da modificare
        $_SESSION['idLib'] = $idLibro;
        $query_records_dep = mysql_query("select prezzo,quantita "
                . "from depositi where venditori_idVenditore = '{$idVenditore}' and libro_idLibro = '{$idLibro}'"); //carica il record dal db
        if (!$query_records_dep) {
            echo 'Impossibile eseguire la ricerca nel db: ' . mysql_error();
            exit;
        }
        ?>


        <div id="form">

            <form method="post" action="index.php?page=venditore">
                <table border="0" cellspacing="3" cellpadding="3">
                    <thead>

                    <input type="hidden" name="cmd" value="edit_submit"/>

                    <tr><th><font face="Arial, Helvetica, sans-serif">Prezzo al pubblico</font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="prezzo" id="prezzo" value="<?php echo mysql_result($query_records_dep, 0, "prezzo"); ?>"/></font></td></tr> 

                    <tr><th><font face="Arial, Helvetica, sans-serif">Quantit&agrave in vendita </font></th>
                        <td align="left"><font face="Arial, Helvetica, sans-serif">
                            <input type="text" name="quantita" id="quantita" value="<?php echo mysql_result($query_records_dep, 0, "quantita"); ?>"/></font></td></tr> 
                    </thead>
                </table>
                <div class="submit">
                    <button type="submit">
                        <img src="../immagini/button_ok.png" class="button_ok"/> Conferma modifiche
                    </button>
                </div>
            </form>
        </div>


    </body>
</html>
