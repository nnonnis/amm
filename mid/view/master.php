<?php
include_once 'PageStruct.php';
?>

<html>
    <head>
        <title><?= $browser->getTitolo() ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="../css/fluido.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../css/generale.css" rel="stylesheet" type="text/css" media="screen" />
    </head>
    <body>
        <div id="pagina">

            <div id="header">
                <!--visualizzazione informazioni utente !-->
                <div id="info">
                    <?php
                    $info = $browser->getInfo();
                    include "$info";
                    ?>
                </div>

                <div id="logo">
                    <?php
                    $logo = $browser->getLogo();
                    require "$logo";
                    ?>
                </div>
                <div id="menu">
                    <?php
                    $menu = $browser->getMenu();
                    require "$menu";
                    ?>
                </div>
            </div>
            <div id="side-bar1">
                <?php
                $sidebar = $browser->getSidebar();
                require "$sidebar";
                ?>
            </div>
            <div id="content">
                <?php
                $content = $browser->getContent();
                require "$content";
                if ($browser->getMessaggioErrore()) {
                    ?>
                    <div id="message">
                        <p><?= $browser->getMessaggioErrore() ?></p>    
                    </div>
                <?php } ?>
            </div>
                    <div id="footer">
                <?php
                $footer = $browser->getFooter();
                require "$footer";
                ?>
            </div>
        </div>

    </body>
</html>