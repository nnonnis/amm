<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Conferma acquisto</title>
    </head>
    <body>
        <p>Conferma acquisto</p>
        <?php
        $idDeposito = $request['idDeposito'];
        $quantita_ord = $request['quantita_ord'];
        $quantita_disp = $request['quantita_disp'];
        $prezzo = $request['prezzo'];
        $sconto = $request['sconto'];
        $idVenditore = $request['venditore'];
        $totale = 0;
        $guadagnoVen = 0; //guadagno del venditore
        if ($quantita_disp < $quantita_ord) { //se la quantità in vendita è minore della quantità che si vuole acquistare
            ?>
            <h2>Quantit&agrave; disponibile minore della quantit&agrave; ordinata.</h2>
            <?php
        } else {
            $totale = $prezzo * $quantita_ord; //importo totale
            $totale_sc = $totale - ((($sconto) * $totale) / 100); //importo totale eventualmente scontato
            $qt_reload = $quantita_disp - $quantita_ord; //nuova quantità, che andrà a rimpiazzare quella precedente nel deposito
            $idUtente = $_SESSION['id']; //contiene id utente;


            //qui metto l'ordine nel carrello
            $mysqli = new mysqli(); 
            $mysqli->connect(Settings::$db_host, Settings::$db_user, Settings::$db_password, Settings::$db_name);
            $mysqli->autocommit(FALSE); //avvio la transazione

            $query = "INSERT INTO carrello (articolo, quantita, totale, clienti_utenti_id)"             //inserisco l'ordine nel carrello
                    . " VALUES ('{$idDeposito}', '{$quantita_ord}', '{$totale_sc}', '{$idUtente}')";
            $result = $mysqli->query($query);


            $query1 = "UPDATE depositi SET quantita='{$qt_reload}' WHERE idDeposito='{$idDeposito}'"; //aggiorno la nuova quantita disponibile per il venditore
            $result1 = $mysqli->query($query1);


            $query2 = "select plafond from venditori where idVenditore='{$idVenditore}'";   //controllo il plafond del venditore
            $result2 = $mysqli->query($query2);


            $guadagnoVen = $totale_sc - (((BaseController::$percOpenBooks) * $totale_sc) / 100); //guadagno netto venditore (tolte le commissioni di Openbooks)

            $valore = $result2->fetch_row();
            $plafond_reload = $valore[0] + $guadagnoVen;

            $query3 = "UPDATE venditori SET plafond='{$plafond_reload}' WHERE idVenditore='{$idVenditore}'"; //aggiorno il plafond venditore
            $result3 = $mysqli->query($query3); //risultato query


            $plafond_reload = 0;

            $query4 = "select plafond from clienti where utenti_id = '{$idUtente}'"; //controllo plafond cliente
            $result4 = $mysqli->query($query4);
            $valore = $result4->fetch_row();
            echo "Nuovo credito cliente -> ".$valore[0];
            if ($valore[0] <= $totale_sc) { //se il credito del cliente è insufficiente
                echo "Credito insufficiente per procedere con l'acquisto. ";
                $mysqli->rollback(); //annullo tutto
                ?>
                    Torna alla Home Page!
                <form method="post" action="index.php?page=venditore">  <!-- link alla home page del cliente -->
                    <button name=”home” type=”submit”>

                        <img src="../immagini/home.png" class="mini_button">
                        <input type="hidden" name="page" value="cliente"/>
                    </button>
                </form>
            <?php
            return;
        }


        $plafond_reload = $valore[0] - $totale_sc; //scalo soldi dal plafond cliente

        $query5 = "UPDATE clienti SET plafond='{$plafond_reload}' WHERE utenti_id='{$idUtente}'"; //aggiorno plafond cliente
        $result5 = $mysqli->query($query5);

        if ($mysqli->errno > 0) { //se la query restituisce un errore
            $mysqli->rollback(); //annullo tutto
            error_log("Errore nella esecuzione della query $mysqli->errno: $mysqli->error", 0); //mostro log
        } else { //altrimenti
            ?>
            <p><h2>Complimenti! L'ordinazione &egrave; andata a buon fine!</h2></p>
        <?php
    }
    $mysqli->autocommit(true); //termino la transazione
}
?>
<p>
    Torna alla Home Page!
<form method="post" action="index.php?page=venditore">
    <button name=”home” type=”submit”>

        <img src="../immagini/home.png" class="mini_button">
        <input type="hidden" name="page" value="cliente"/>
    </button>
</form>
</p>
</body>
</html>
