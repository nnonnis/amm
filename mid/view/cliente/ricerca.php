<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ricerca libro</title>
    </head>
    <body>
        <p>Ricerca libro</p>
        <?php
        $titolo = $request['titolo']; //memorizza il titolo da cercare
        echo "Risultati trovati per " . $titolo . ":";
        $idLibro = ClienteController::ricercaIdLibroDaTitolo($titolo); //calcola id da titolo
        $query_search_depositi = mysql_query("select venditori_idVenditore, idDeposito, costo, prezzo, sconto, "
                . "quantita from depositi where libro_idLibro = '{$idLibro}'"); //cerca risultati da id libro
        if (!$query_search_depositi) {
            echo 'Impossibile eseguire la ricerca del libro nel db: ' . mysql_error();
            exit;
        }
        $numero_libri = mysql_num_rows($query_search_depositi);
        if ($numero_libri == 0) { //se non sono stati trovati risultati, mostra messaggio
            ?>
            <p>Non sono disponibili libri con questo titolo.</p> <?php
        } else {
            ?>
            <p>Sono stati trovati <?php echo $numero_libri . " risultati"; ?></p>
            <table border="1" cellspacing="1" cellpadding="1">
                <thead>
                    <tr>
                        <th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Prezzo</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Sconto (%)</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Quantita' disponibile</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Compra</font></th>
                    </tr>
                </thead>
                <?php
                $i = 0;
                while ($i < $numero_libri) {
                    $idDeposito = mysql_result($query_search_depositi, $i, "idDeposito");
                    $prezzo = mysql_result($query_search_depositi, $i, "prezzo");
                    $sconto = mysql_result($query_search_depositi, $i, "sconto");
                    $quantita = mysql_result($query_search_depositi, $i, "quantita");
                    $idVenditore = mysql_result($query_search_depositi, $i, "venditori_idVenditore");
                    if($quantita>0){
                    ?>
                    <tr>
                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $titolo; ?></font></td>

                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $prezzo; ?></font></td>

                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php
                            if ($sconto > 0)
                                echo $sconto;
                            else
                                echo "Non applicato"
                                ?></font></td>

                        <td align="center"><font face="Arial, Helvetica, sans-serif"><?php
                            if ($quantita > 10)
                                echo "Pi&ugrave; di 10";
                            else
                                echo $quantita;
                            ?></font></td>

                        <td align='center'>
                            <form method="post" action="index.php?page=cliente">
                                Quantit&agrave; <input type="text" name="quantita_ord" id="quantita_ord" value="1"/>
                                <button name=”buy” type=”submit”>
                                    <img src="../immagini/aggiungi_carrello.png" class="mini_button">
                                    <input type="hidden" name="subpage" value="compra"/>
                                    <input type="hidden" name="idDeposito" value="<?php echo $idDeposito; ?>"/>
                                    <input type="hidden" name="quantita_disp" value="<?php echo $quantita; ?>"/>
                                    <input type="hidden" name="prezzo" value="<?php echo $prezzo; ?>"/>
                                    <input type="hidden" name="venditore" value="<?php echo $idVenditore; ?>"/>
                                    <input type="hidden" name="sconto" value="<?php echo $sconto; ?>"/>
                                </button>
                            </form>
                        </td>
                    </tr>
                    <?php
                    }
                    
                    $i++;
                }
                ?>
            </table>
        <?php }
        ?>
    </body>
</html>
