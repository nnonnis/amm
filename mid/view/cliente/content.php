<?php
include_once basename(__DIR__) . '/../controller/BaseController.php';
include_once basename(__DIR__) . '/../view/connessione.php';

switch ($browser->getSottoPagina()) {

    case 'compra':
        include 'compra.php';
        break;

    case 'anagrafica':
        include 'anagrafica.php';
        break;

    case 'cronologia':
        include 'cronologia.php';
        break;

    case 'ricarica':
        include 'ricarica.php';
        break;

    case 'ricarica1':
        include 'ricarica1.php';
        break;

    case 'ricerca':
        include 'ricerca.php';
        break;
        ?>

    <?php default: ?>
        <!-- Home page del cliente-->
        Benvenuto, <?php echo $row['nome']; ?> <?php echo $row['cognome']; ?>. <!-- visualizza nome e cognome cliente -->

        <div id="notice"><h3>Hai ancora a disposizione   
                <?php
                echo " &euro; " . BaseController::getCreditoCli($user) . ". Cosa aspetti!"; //visualizza credito cliente
                ?>
            </h3>
        </div>
        <form method="post" action="index.php?page=cliente">
            <input type="hidden" name="subpage" value="ricerca"/>
            <label for="titolo">Cerca titolo: </label>
            <input type="text" name="titolo" id="titolo"/>
            <div class="submit">
                <button type="submit">
                    <img src="../immagini/cerca_libro.png" class="button_ok"/> Ricerca libro
                </button>
            </div>
        </form>
        <hr/>

        <?php
//MOSTRO TUTTI I LIBRI IN OFFERTA NELLA HOME PAGE DEL VENDITORE
        $query_search_offerte = mysql_query("select titolo, venditori_idVenditore, idDeposito, prezzo, sconto, "
                . "quantita from depositi, libro where sconto>'0' and idLibro=libro_idLibro");
        if (!$query_search_offerte) {
            echo 'Impossibile eseguire la ricerca del libro nel db: ' . mysql_error();
            exit;
        }
        $numero_libri = mysql_num_rows($query_search_offerte);
        if ($numero_libri == 0) {
            ?>
            <p>Nessun libro in offerta da mostrare nella Home.</p> <?php
        } else {
            ?>
            <p>Hei! Ci sono <?php echo $numero_libri . " libri in offerta!"; ?></p>
            <table border="1" cellspacing="1" cellpadding="1">
                <thead>
                    <tr>
                        <th><font face="Arial, Helvetica, sans-serif">Titolo</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Prezzo</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Sconto (%)</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Quantita' disponibile</font></th>
                        <th><font face="Arial, Helvetica, sans-serif">Compra</font></th>
                    </tr>
                </thead>
                <?php
                $i = 0;
                while ($i < $numero_libri) {
                    $titolo = mysql_result($query_search_offerte, $i, "titolo");
                    $idDeposito = mysql_result($query_search_offerte, $i, "idDeposito");
                    $prezzo = mysql_result($query_search_offerte, $i, "prezzo");
                    $sconto = mysql_result($query_search_offerte, $i, "sconto");
                    $quantita = mysql_result($query_search_offerte, $i, "quantita");
                    $idVenditore = mysql_result($query_search_offerte, $i, "venditori_idVenditore");
                    if ($quantita > 0) {
                        ?>
                        <tr>
                            <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $titolo; ?></font></td>

                            <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $prezzo; ?></font></td>

                            <td align="center"><font face="Arial, Helvetica, sans-serif"><?php echo $sconto; ?></font></td>

                            <td align="center"><font face="Arial, Helvetica, sans-serif"><?php
                                if ($quantita > 10)
                                    echo "Pi&ugrave; di 10";
                                else
                                    echo $quantita;
                                ?></font></td>

                            <td align='center'>
                                <form method="post" action="index.php?page=cliente">
                                    Quantit&agrave; <input type="text" name="quantita_ord" id="quantita_ord" value="1"/>
                                    <button name=”buy” type=”submit”>
                                        <img src="../immagini/aggiungi_carrello.png" class="mini_button">
                                        <input type="hidden" name="subpage" value="compra"/>
                                        <input type="hidden" name="idDeposito" value="<?php echo $idDeposito; ?>"/>
                                        <input type="hidden" name="quantita_disp" value="<?php echo $quantita; ?>"/>
                                        <input type="hidden" name="prezzo" value="<?php echo $prezzo; ?>"/>
                                        <input type="hidden" name="venditore" value="<?php echo $idVenditore; ?>"/>
                                        <input type="hidden" name="sconto" value="<?php echo $sconto; ?>"/>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    $i++;
                }
                ?>
            </table>
            <?php
            break;
        }
}
?>

