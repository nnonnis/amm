<?php


class PageStruct {

    const get = 'get';

    const post = 'post';


    private $titolo;
    
    private $info;

    private $logo;

    private $menu;

    private $sidebar;

    private $content;
    
    private $footer;

    private $messaggioErrore;

    private $messaggioConferma;

    private $pagina;

    private $sottoPagina;

    private $impToken;

    public function __construct() {
        ;
    }


    public function getTitolo() {
        return $this->titolo;
    }
    
    public function getInfo() {
        return $this->info;
    }
    
    public function setInfo($info) {
        $this->info = $info;
    }
    
        public function getFooter() {
        return $this->footer;
    }


    public function setTitolo($titolo) {
        $this->titolo = $titolo;
    }
            

    public function setLogo($logo) {
        $this->logo = $logo;
    }


    public function getLogo() {
        return $this->logo;
    }

    public function getMenu() {
        return $this->menu;
    }

 
    public function setMenu($menu) {
        $this->menu = $menu;
    }


    public function getSidebar() {
        return $this->sidebar;
    }
    
    public function setFooter($footer){
        $this->footer = $footer;
    }


    public function setSidebar($sidebar) {
        $this->sidebar = $sidebar;
    }


    public function setContent($content) {
        $this->content = $content;
    }

    public function getContent() {
        return $this->content;
    }
    

    public function getMessaggioErrore() {
        return $this->messaggioErrore;
    }


    public function setMessaggioErrore($msg) {
        $this->messaggioErrore = $msg;
    }


    public function getSottoPagina() {
        return $this->sottoPagina;
    }


    public function setSottoPagina($pag) {
        $this->sottoPagina = $pag;
    }


    public function getMessaggioConferma() {
        return $this->messaggioConferma;
    }


    public function setMessaggioConferma($msg) {
        $this->messaggioConferma = $msg;
    }


    public function getPagina() {
        return $this->pagina;
    }


    public function setPagina($pagina) {
        $this->pagina = $pagina;
    }

    

    public function setImpToken($token) {
        $this->impToken = $token;
    }


    public function scriviToken($pre = '', $method = self::get) {
        $imp = BaseController::impersonato;
        switch ($method) {
            case self::get:
                if (isset($this->impToken)) {
                    return $pre . "$imp=$this->impToken";
                }
                break;

            case self::post:
                if (isset($this->impToken)) {
                    return "<input type=\"hidden\" name=\"$imp\" value=\"$this->impToken\"/>";
                }
                break;
        }

        return '';
    }

}

?>
