<?php

include_once basename(__DIR__) . '/../view/PageStruct.php';
include_once basename(__DIR__) . '/../model/Data_manager.php';
include_once basename(__DIR__) . '/../controller/BaseController.php';
include_once basename(__DIR__) . '/../controller/VendorController.php';
include_once basename(__DIR__) . '/../view/connessione.php';
include_once basename(__DIR__) . '/../controller/ClienteController.php';


FrontController::dispatch($_REQUEST);

class FrontController {

    static function dispatch(&$request) {
        session_start();

        if (isset($request["page"])) { //è stata richiesta una pagina

            switch ($request["page"]) {

                case "login":

                    $controller = new BaseController();
                    $controller->handleInput($request, $_SESSION);

                    break;

                case "admin":
                    //$controller = new AdminController(); //non implementato

                    break;


                case "venditore":
                    $controller = new VendorController();
                    $controller->handleInput($request, $_SESSION);
                    break;


                case "cliente":
                    $controller = new ClienteController();
                    $controller->handleInput($request, $_SESSION);
                    break;



                default: /* pagina non trovata */
                    $browser = new PageStruct();
                    $browser->setTitolo("Pagina non trovata");
                    $browser->setInfo('../mid/view/login/infoUtente.php');
                    $browser->setMenu('../mid/view/def/menu.php');
                    $browser->setLogo('../mid/view/def/logo.php');
                    $browser->setSidebar('../mid/view/def/sidebar.php');
                    $browser->setContent('../mid/view/def/not-found-content.php');
                    $browser->setFooter('../mid/view/def/footer.php');

                    break;
            }
        } else {
            //home page
            $browser = new PageStruct();
            $browser->setTitolo("Home");
            $browser->setInfo('../mid/view/login/infoUtente.php');
            $browser->setMenu('../mid/view/home/menu.php');
            $browser->setLogo('../mid/view/home/logo.php');
            $browser->setSidebar('../mid/view/home/sidebar.php');
            $browser->setContent('../mid/view/home/content.php');
            $browser->setFooter('../mid/view/home/footer.php');
        }
        include_once 'view/master.php';
    }

}

?>